package oop.lab1;

//TODO Write class Javadoc
/**
 * A simple model for a student with name and id.
 * 
 * @author Phasin Sarunpornkul
 */
public class Student extends Person {
	/* the student's id must be the numeric character. */
	private long id;
	
	//TODO Write constructor Javadoc
	/**
	 * Initialize a new Student object.
	 * @param name is the name of the new Student
	 * @param id is the identify number of the new Student
	 */
	public Student(String name, long id) {
		super(name); // name is managed by Person
		this.id = id;
	}

	/** return a string representation of this Student. */
	public String toString() {
		return String.format("Student %s (%d)", getName(), id);
	}

	//TODO Write equals
	public boolean equals(Object obj) {
		if (obj==null){
			return false;
		}
		if(obj.getClass() != this.getClass()){
			return false;
		}
		Student other = (Student)obj;
		if(id==other.id){
			return true;
		}
		return false;
	}
}
